# hack-ideas

## Project setup
```
docker build .
docker run -p 8000:8080 <image-id>

```

## Development server
[http://localhost:8000](http://localhost:8000)


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
