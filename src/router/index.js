import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import CreateChallenges from '../components/CreateChallenges.vue'
import ChallengeDetails from '../components/ChallengeDetails.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/add-challenges',
    name: 'new-challenge',
    component: CreateChallenges
  },
  {
    path: '/challenges-details/:id',
    name: 'challenge-detail',
    component: ChallengeDetails,
    props: route => ({id: route.params.id})
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
