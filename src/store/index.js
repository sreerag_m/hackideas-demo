import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex)

export const store = new Vuex.Store({
  plugins: [createPersistedState({
    paths: ['challengeList', 'empId']
  })],
  state: {
    empId: '',
    challengeList: [],
    sortDirection: 'desc'
  },
  mutations: {
    addChallenges (state, data) {
      state.challengeList.unshift(data)
    },
    addUserId (state, data) {
      state.empId = data
    },
    removeUser (state) {
      state.empId = ''
    },
    removeChallenge(state,id) {
      for(let i= 0; i < state.challengeList.length; i++) {
        if (state.challengeList[i].id == id && state.challengeList[i].empId == state.empId) {
          state.challengeList.splice(i,1)
        }
      }
    },
    addVoteCount(state, id) {
      state.challengeList.find(challenge => {
        if (challenge.id === id) {
          challenge.likedEmps.push(state.empId)
        }
      })
    }
  },
  getters: {
    getChallenge: state => id => {
      return state.challengeList.find(challenge=> challenge.id == id)
    },
    sortedChallengeArray: state => type => {
      return state.challengeList.sort((p1,p2) => {
        let modifier = 1;
        if(state.sortDirection === 'desc') modifier = -1;
        if (type === 'id') if(p1[type] < p2[type]) return -1 * modifier; if(p1[type] > p2[type]) return 1 * modifier;
        if (type === 'vote') {
          if(p1.likedEmps.length < p2.likedEmps.length) return -1 * modifier; if(p1.likedEmps.length > p2.likedEmps.length) return 1 * modifier;
        }
        return 0;
      })
    }
  }
})