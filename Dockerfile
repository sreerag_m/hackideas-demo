FROM node:14-alpine

ENV APP_ROOT /usr/src/app

WORKDIR ${APP_ROOT}

COPY . ${APP_ROOT}

RUN npm install

ENV HOST 0.0.0.0
EXPOSE 8080

CMD [ "npm", "run", "serve" ]